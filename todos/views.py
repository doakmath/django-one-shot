from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodosForm, NewItemForm

# Create your views here.


def todo_list_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_list_detail,
    }
    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodosForm(request.POST)
        if form.is_valid():
            todos = form.save()
            todos.save()
            return redirect("todo_list_detail", id=todos.id)
    else:
        form = TodosForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodosForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodosForm(instance=post)
    context = {
        "todos": post,
        "todos_form": form,
    }
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    post = TodoList.objects.get(id=id)
    if request.method == "POST":
        post.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = NewItemForm(request.POST)
        if form.is_valid():
            new_item = form.save()
            return redirect("todo_list_detail", id=new_item.list.id)
    else:
        form = NewItemForm()
    context = {
        "item_form": form
    }
    return render(request, "todos/create_item.html", context)

def todo_item_edit(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = NewItemForm(request.POST, instance = post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", post.list.id)
    else:
        form = NewItemForm(instance=post)
    context = {
        "item": post,
        "item_form": form,
        }
    return render(request, "todos/create_item.html", context)
